package Lib;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;


/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Apr 17, 2019.
 */
public class LocalDriverFactory {

	static WebDriver driver=null;
	
	public static WebDriver createInstance(String browser) {
		
		if(browser.toLowerCase().contains("chrome")) {
			System.setProperty("webdriver.chrome.driver", "../../resources/config/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			driver= new ChromeDriver(options);
			return driver;
		}
	
		if(browser.toLowerCase().contains("firefox")) {
			FirefoxProfile profile= new FirefoxProfile();
			//driver.manage().window().maximize(); Add dependency for window in POM
			return driver;
		}
		return driver;
		
	
	}
}
