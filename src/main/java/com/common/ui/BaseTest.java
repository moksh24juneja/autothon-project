package com.common.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;



import Lib.LocalDriverFactory;
import Services.hitRestService;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Apr 17, 2019.
 */
public class BaseTest {
	
	public static WebDriver driver;
	
	
	@BeforeSuite(alwaysRun=true)
	public void setUp(final ITestContext context) {
		
		
	}
	
	@Parameters("browser")
	@BeforeMethod(alwaysRun=true)
	public void setUpSelenium(String browser) throws InterruptedException {
		
		try {
		LocalDriverFactory.createInstance(browser);
		}catch(Exception e) {
			System.out.println("Parameter passed are not correct");
		}
		
		driver.get("http://thedemosite.co.uk/");
		Thread.sleep(2000);
  	  
  	  //new WebDriverWait(driver, 25).until(ExpectedConditions.presenceOfElementLocated(HomePageLocators.loginpage.getLocator()));
	}

	   @AfterMethod(alwaysRun=true)
	      public void closeSetUp() {
	    	  try {
	    		  
	    		  Thread.sleep(1000);
	    		  driver.quit();
	    	  }catch(Exception e) {}
	      }
	      
	      @AfterTest(alwaysRun=true)
	      public void closeBrowser() {
	    	  try {
	    		  Thread.sleep(1000);
	    		  driver.close();
	    	  }catch(Exception e) {}
	      }
}
