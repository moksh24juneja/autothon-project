package Scripts;


import java.util.Hashtable;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.common.ui.BaseTest;

import Services.FibonnaciResults;
import Services.hitRestService;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Apr 17, 2019.
 */
public class TestScripts extends BaseTest{
	
	
	ScriptSetUP setupscript=null;
	
	@Test(alwaysRun=true,dataProvider="data-provider")
	public void runTestScenarios(int key, String value) {
		
	    System.out.println(key+ " "+ value);
		
		;
		
	}
	
	@DataProvider(name="data-provider")
	public Object[][] dataProviderMethod(){
		Object[][] obj= new Object[2][2];
		
		//Pass JSON response of fibonacci class to HashTable as it is thread-safe
		Hashtable<Integer, Object> map1= new Hashtable<Integer, Object>();
		map1.putAll(hitRestService.getRestResponse());
		
		for(int i=0; i<2; i++) {
		int j=0;
		//To get random keys from the map
		Integer[] keys= map1.keySet().toArray(new Integer[map1.size()]);
		Integer key= keys[new Random().nextInt(keys.length)];
		
		//Fetch values from keys and pass to object
		String values= map1.get(key).toString();
		
		obj[i][j]=key;
		 j=1;
		obj[i][j]=values;
		map1.remove(key);
		}
		System.out.println(map1);
		return obj;
	
	}
	

}
