package com.common.ui;

import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import Lib.LocalDriverFactory;
import Services.hitRestService;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Apr 17, 2019.
 */
public class BaseTest {
	
	public static WebDriver driver;
	
	protected ConcurrentHashMap<Integer, Object> map1= new ConcurrentHashMap<Integer, Object>();
	
	@BeforeSuite(alwaysRun=true)
	public void getJSONRecords(final ITestContext context) throws InterruptedException {
		hitRestService.getRestResponse();
		
	}
	
    @Parameters("browser")
	@BeforeTest(alwaysRun=true)
	public void setUpSelenium(String browser) throws InterruptedException, MalformedURLException {
		
		System.out.println(browser);
		
		driver=LocalDriverFactory.createInstance(browser);	
		//driver.get("url");
		Thread.sleep(6000);
  	  
  	  //new WebDriverWait(driver, 25).until(ExpectedConditions.presenceOfElementLocated(HomePageLocators.loginpage.getLocator()));
	}

	   @AfterMethod(alwaysRun=true)
	      public void closeSetUp() {
	    	  try {
	    		  
	    		  Thread.sleep(1000);
	    		 // driver.quit();
	    	  }catch(Exception e) {}
	      }
	      
	      @AfterTest(alwaysRun=true)
	      public void closeBrowser() {
	    	  try {
	    		  Thread.sleep(1000);
	    		 // driver.close();
	    	  }catch(Exception e) {}
	      }
}
