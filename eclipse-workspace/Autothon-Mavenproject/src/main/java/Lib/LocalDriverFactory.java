package Lib;

import java.net.MalformedURLException;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;






/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Apr 17, 2019.
 */
public class LocalDriverFactory {

	static WebDriver driver=null;
	
	public static WebDriver createInstance(String browser) throws MalformedURLException {
		
		if(browser.toLowerCase().contains("chrome")) {
			
			
			Map<String, Object> prefs = new HashMap<String, Object>();
			//To Turns off multiple download warning
			prefs.put("profile.default_content_settings.popups", 0);

			prefs.put( "profile.content_settings.pattern_pairs.*.multiple-automatic-downloads", 1 );

			//Turns off download prompt
			prefs.put("download.prompt_for_download", false);
			                    prefs.put("credentials_enable_service", false);
			//To Stop Save password propmts
			prefs.put("password_manager_enabled", false);

			ChromeOptions options = new ChromeOptions();
			options.addArguments("chrome.switches","--disable-extensions");
			//To Disable any browser notifications
			options.addArguments("--disable-notifications");
			//To disable yellow strip info bar which prompts info messages
			options.addArguments("disable-infobars");

			options.setExperimentalOption("prefs", prefs);
			options.addArguments("--test-type");
			options.setExperimentalOption("useAutomationExtension", false);
			System.setProperty("webdriver.chrome.driver", "D:\\D Moksh\\JAR Files\\chromedriver.exe");

			options.addArguments("disable-extensions");
			options.addArguments("--no-sandbox");
	        options.addArguments("--disable-dev-shm-usage");
		    options.addArguments("--start-maximized");
		    driver = new ChromeDriver( options);
			return driver;
		}
	
		if(browser.toLowerCase().contains("firefox")) {

			System.setProperty("webdriver.gecko.driver","D:\\D Moksh\\JAR Files\\geckodriver.exe");
			WebDriver driver = new FirefoxDriver();
			return driver;
		}
		return driver;
		
	
	}
}
